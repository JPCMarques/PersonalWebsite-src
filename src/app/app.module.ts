import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { Routes, RouterModule, Router } from '@angular/router';

import { AppComponent } from './app.component';
import { BlogComponent } from './content-pages/blog/blog.component';
import { NovelsComponent } from './content-pages/novels/novels.component';
import { MyAppsComponent } from './content-pages/my-apps/my-apps.component';
import { AboutComponent } from './content-pages/about/about.component';
import { NavbarComponent } from './shared/navbar/navbar.component';
import { HomepageComponent } from './content-pages/homepage/homepage.component';
import { NewsContainerComponent } from './shared/news-container/news-container.component';
import { FooterComponent } from './shared/footer/footer.component';
import { MyAppsContainerComponent } from './content-pages/my-apps/my-apps-container/my-apps-container.component';
import { PostOrganizerComponent } from './content-pages/blog/post-organizer/post-organizer.component';
import { PostDisplayComponent } from './content-pages/blog/post-display/post-display.component';
import { HandlebarsService } from './services/handlebars.service';
import { DataService } from './services/data.service';
import { UtilService } from './services/util.service';
import { BlogPostService } from './services/data/blog-post.service';
import { SafeHtmlPipe } from './pipes/safe-html.pipe';
import { MonthlyOrganizerComponent } from './content-pages/blog/post-organizer/monthly-organizer/monthly-organizer.component';
import { AppService } from './services/data/app.service';
import { NovelService } from './services/data/novel.service';
import { GeneralNewsService } from './services/data/general-news.service';
import { NovelUpdatesService } from './services/data/novel-updates.service';
import { NewsDataComponent } from './shared/news-data/news-data.component';
import { CvComponent } from './content-pages/about/cv/cv.component';
import { StoryMapComponent } from './content-pages/novels/smaps/story-map/story-map.component';
import { StoryMapSidebarComponent } from './content-pages/novels/smaps/story-map-sidebar/story-map-sidebar.component';
import { StoryMapDisplayComponent } from './content-pages/novels/smaps/story-map-display/story-map-display.component';
import { NovelPageContainerComponent } from './content-pages/novels/novel-page-container/novel-page-container.component';
// tslint:disable-next-line:max-line-length
import { GroupedSidebarElementComponent } from './content-pages/novels/smaps/story-map-sidebar/grouped-sidebar-element/grouped-sidebar-element.component';

const routes: Routes = [
  {path: '', component: HomepageComponent },
  {path: 'about', component: AboutComponent},
  {path: 'about/cv', component: CvComponent},
  {path: 'novels', component: NovelPageContainerComponent, children: [
    {path: '', component: NovelsComponent},
    {path: 'storyMap', redirectTo: ''},
    {path: 'storyMap/:id', component: StoryMapComponent}
  ]},
  {path: 'apps', component: MyAppsComponent},
  {path: 'blog', component: BlogComponent, children: [
    {path: '', component: PostDisplayComponent},
    {path: ':year', component: PostDisplayComponent},
    {path: ':year/:month', component: PostDisplayComponent},
    {path: ':year/:month/:id', component: PostDisplayComponent}
  ]},
  {path: '**', redirectTo: ''}
];

@NgModule({
  declarations: [
    AppComponent,
    BlogComponent,
    NovelsComponent,
    MyAppsComponent,
    AboutComponent,
    NavbarComponent,
    HomepageComponent,
    NewsContainerComponent,
    FooterComponent,
    MyAppsContainerComponent,
    PostOrganizerComponent,
    PostDisplayComponent,
    SafeHtmlPipe,
    MonthlyOrganizerComponent,
    NewsDataComponent,
    CvComponent,
    StoryMapComponent,
    StoryMapSidebarComponent,
    StoryMapDisplayComponent,
    NovelPageContainerComponent,
    GroupedSidebarElementComponent,
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(routes),
  ],
  providers: [HandlebarsService, DataService, BlogPostService, UtilService,
              AppService, NovelService, GeneralNewsService, NovelUpdatesService],
  bootstrap: [AppComponent]
})
export class AppModule { }
