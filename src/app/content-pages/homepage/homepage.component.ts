import { Component, OnInit } from '@angular/core';
import { DataService } from '../../services/data.service';
import { GeneralNewsService } from '../../services/data/general-news.service';
import { Data } from '../../shared/const/interfaces/data';

@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.css']
})
export class HomepageComponent implements OnInit {
  news: Data[];

  constructor(private generalNewsService: GeneralNewsService) { }

  ngOnInit() {
    this.generalNewsService.newsUpdated.subscribe(() => {
      this.news = this.generalNewsService.news;
    });
    this.news = this.generalNewsService.news;

    console.log(this.news);
  }

}
