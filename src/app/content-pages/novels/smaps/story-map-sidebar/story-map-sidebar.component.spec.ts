import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StoryMapSidebarComponent } from './story-map-sidebar.component';

describe('StoryMapSidebarComponent', () => {
  let component: StoryMapSidebarComponent;
  let fixture: ComponentFixture<StoryMapSidebarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StoryMapSidebarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StoryMapSidebarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
