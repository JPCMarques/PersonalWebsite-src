import { Component, OnInit, Input } from '@angular/core';
import { ILevelData } from '../../../../../shared/const/interfaces/data';
import { UUIDService } from '../../../../../services/uuid.service';
import { StoryMapMetadataService } from '../../../../../services/data/storyMap/story-map-metadata.service';

@Component({
  selector: 'app-grouped-sidebar-element',
  templateUrl: './grouped-sidebar-element.component.html',
  styleUrls: ['./grouped-sidebar-element.component.css']
})
export class GroupedSidebarElementComponent implements OnInit {
  @Input() levelData: ILevelData;
  @Input() parentIndex: string;

  constructor(private smapMetaService: StoryMapMetadataService) {
    smapMetaService.smapMetaCollapseID.subscribe((id) => {
      console.log(`parent: ${this.parentIndex}; clickID: ${id}`);
      // if (this.parentIndex === id) {
      //   this.isCollapsed = !this.isCollapsed;
      // }
    });
  }

  ngOnInit() { 
    console.log(this.parentIndex);
  }

  toggleCollapse() {
    this.smapMetaService.handleCollapse(this.parentIndex);
  }
}
