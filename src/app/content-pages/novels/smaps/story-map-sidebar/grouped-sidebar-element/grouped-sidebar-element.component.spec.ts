import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GroupedSidebarElementComponent } from './grouped-sidebar-element.component';

describe('GroupedSidebarElementComponent', () => {
  let component: GroupedSidebarElementComponent;
  let fixture: ComponentFixture<GroupedSidebarElementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GroupedSidebarElementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GroupedSidebarElementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
