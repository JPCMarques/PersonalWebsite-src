import { Component, OnInit } from '@angular/core';
import { StoryMapMetadataService } from '../../../../services/data/storyMap/story-map-metadata.service';
import { ISMapMetadata } from '../../../../shared/const/interfaces/data';
import { FirebaseConfig } from '../../../../shared/const/config';
import * as uuid from 'uuid';

@Component({
    selector: 'app-story-map-sidebar',
    templateUrl: './story-map-sidebar.component.html',
    styleUrls: ['./story-map-sidebar.component.css']
})
export class StoryMapSidebarComponent implements OnInit {

    smapMeta: ISMapMetadata;
    lastActiveIndex: number;

    constructor(private storyMapMetadataService: StoryMapMetadataService) {
        this.storyMapMetadataService.smapMetaCollectionUpdated.subscribe(() => {
            this.smapMeta = this.storyMapMetadataService.getMetadata(FirebaseConfig.ConstantKeys.Novels.BookHunters);
        });
    }

    ngOnInit() {
    }

    toggleCollapse(index: number): void {
        this.lastActiveIndex = index;
    }

}
