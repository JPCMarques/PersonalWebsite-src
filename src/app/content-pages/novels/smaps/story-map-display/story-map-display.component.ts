import { Component, OnInit, Input } from '@angular/core';
import { ISMap, ISMapLevel } from '../../../../shared/const/interfaces/data';
import { HandlebarsService } from '../../../../services/handlebars.service';
import { Templates } from '../../../../shared/const/templates';
import { StoryMapService } from '../../../../services/data/storyMap/story-map.service';
import { ActivatedRoute, Router } from '../../../../../../node_modules/@angular/router';
import { UtilService } from '../../../../services/util.service';

@Component({
    selector: 'app-story-map-display',
    templateUrl: './story-map-display.component.html',
    styleUrls: ['./story-map-display.component.css']
})
export class StoryMapDisplayComponent implements OnInit {
    private smapId: string;
    private smapStack: ISMap[] = [];
    private queryParams: object;
    private qpAnalyzed = false;

    @Input() storyMap: ISMap;

    formattedHtml: string;

    constructor(private storyMapService: StoryMapService,
        private utilService: UtilService,
        private activatedRoute: ActivatedRoute,
        private router: Router) {

        this.activatedRoute.params.subscribe((params) => {
            this.smapId = params['id'];
        });

        this.queryParams = this.activatedRoute.snapshot.queryParams;

        this.activatedRoute.queryParams.subscribe((qParams) => {
            this.queryParams = qParams;
        });

        this.storyMapService.storyMapsUpdated.subscribe(() => {
            this.storyMap = this.storyMapService.getStoryMap(this.smapId);
            this.smapStack.push({ ... this.storyMap });
            if (!this.qpAnalyzed && this.queryParams !== undefined && Object.keys(this.queryParams).length !== 0) {
                this.qpNav();
                this.qpAnalyzed = true;
            }
        });
    }

    ngOnInit() {
    }

    qpNav() {

        const tQParams = {};
        let tSmap = this.storyMap;
        // Iterate through the query params (angular keeps the input order)
        Object.keys(this.queryParams).some((qparam) => {
            if (!tSmap.subLevels) { return false; }
            let subLevel: ISMapLevel;

            if (!(tSmap.subLevels.some((level) => {
                if (level.id === qparam) {
                    subLevel = level;
                    return true;
                }
                return false;
            }))) {return false; }

            return !subLevel.members.some((member) => {
                if (member.title === this.queryParams[qparam]) {
                    tSmap = member;
                    this.smapStack.push(tSmap);
                    tQParams[qparam] = this.queryParams[qparam];
                    return true;
                }
                return false;
            });
        });

        this.utilService.destructiveCopy(tSmap, this.storyMap, {clearList: ['subLevels']});
        this.router.navigate([], {queryParams: tQParams});
    }

    navigateToSubpage(subpage: ISMap) {
        this.storyMap = subpage;
        this.smapStack.push({ ... this.storyMap });
        const qParams = this.utilService.appendQueryParams(this.queryParams, subpage);
        this.router.navigate([], { queryParams: qParams });
    }

    breadCrumbsNavigate(index: number) {
        this.smapStack.splice(index + 1);
        this.storyMap = this.smapStack[index];
        if (index === 0) {
            this.router.navigate([], { queryParams: {} });
        } else {
            let queryParams = {};
            for (let i = 1; i < this.smapStack.length; i++) {
                queryParams = this.utilService.appendQueryParams(queryParams, this.smapStack[i]);
            }
            this.router.navigate([], { queryParams: queryParams });
        }
    }

}
