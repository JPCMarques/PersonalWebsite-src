import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StoryMapDisplayComponent } from './story-map-display.component';

describe('StoryMapDisplayComponent', () => {
  let component: StoryMapDisplayComponent;
  let fixture: ComponentFixture<StoryMapDisplayComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StoryMapDisplayComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StoryMapDisplayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
