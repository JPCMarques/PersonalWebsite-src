import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '../../../../../../node_modules/@angular/router';
import { ISMap } from '../../../../shared/const/interfaces/data';
import { StoryMapService } from '../../../../services/data/storyMap/story-map.service';

@Component({
  selector: 'app-story-map',
  templateUrl: './story-map.component.html',
  styleUrls: ['./story-map.component.css']
})
export class StoryMapComponent implements OnInit {
  private smapId: string;
  smap: ISMap;

  constructor(private storyMapService: StoryMapService, private activatedRoute: ActivatedRoute) {
    this.activatedRoute.params.subscribe((params) => {
      this.smapId = params['id'];
    });

    this.storyMapService.storyMapsUpdated.subscribe(() => {
      this.smap = this.storyMapService.getStoryMap(this.smapId);
    });
  }

  ngOnInit() {
  }

}
