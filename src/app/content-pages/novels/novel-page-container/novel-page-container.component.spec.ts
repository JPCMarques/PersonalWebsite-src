import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NovelPageContainerComponent } from './novel-page-container.component';

describe('NovelPageContainerComponent', () => {
  let component: NovelPageContainerComponent;
  let fixture: ComponentFixture<NovelPageContainerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NovelPageContainerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NovelPageContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
