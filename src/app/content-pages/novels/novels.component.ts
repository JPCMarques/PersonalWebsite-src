import { Component, OnInit } from '@angular/core';
import { HandlebarsService } from '../../services/handlebars.service';
import { NovelService } from '../../services/data/novel.service';
import { isNullOrUndefined } from 'util';
import { NovelUpdatesService } from '../../services/data/novel-updates.service';
import { Templates } from '../../shared/const/templates';
import { Novel, Data } from '../../shared/const/interfaces/data';

@Component({
  selector: 'app-novels',
  templateUrl: './novels.component.html',
  styleUrls: ['./novels.component.css']
})
export class NovelsComponent implements OnInit {
  news: Data[] = [];
  novels: string[] = [];

  constructor(private novelService: NovelService,
              private novelUpdatesService: NovelUpdatesService,
              private handlebarsService: HandlebarsService) { }

  ngOnInit() {
    this.novelService.novelsUpdated.subscribe(() => {
      this.novels = this.novelService.novels.map((novel: Novel) => {
        return this.handlebarsService.useCustomTemplate(Templates.Novel.id, novel);
      });
    });
    if (!isNullOrUndefined(this.novelService.novels)) {
      this.novels = this.novelService.novels.map((novel: Novel) => {
        return this.handlebarsService.useCustomTemplate(Templates.Novel.id, novel);
      });
    }

    this.novelUpdatesService.novelUpdatesUpdated.subscribe(() => {
      this.news = this.novelUpdatesService.novelUpdates;
    });
    this.news = this.novelUpdatesService.novelUpdates;
  }

}
