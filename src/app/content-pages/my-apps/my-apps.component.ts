import { Component, OnInit } from '@angular/core';
import { AppService } from '../../services/data/app.service';
import { HandlebarsService } from '../../services/handlebars.service';
import { isNullOrUndefined } from 'util';
import { Templates } from '../../shared/const/templates';
import { App } from '../../shared/const/interfaces/data';

@Component({
  selector: 'app-my-apps',
  templateUrl: './my-apps.component.html',
  styleUrls: ['./my-apps.component.css']
})
export class MyAppsComponent implements OnInit {
  apps: string[] = [];

  constructor(private appService: AppService, private handlebarsService: HandlebarsService) { }

  ngOnInit() {
    this.appService.appDataUpdated.subscribe(() => {
      this.apps = this.appService.apps.map((app: App) => {
        return this.handlebarsService.useCustomTemplate(Templates.App.id, app);
      });
    });
    if (!isNullOrUndefined(this.appService.apps)) {
      this.apps = this.appService.apps.map((app: App) => {
        return this.handlebarsService.useCustomTemplate(Templates.App.id, app);
      });
    }
  }

}
