import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { HandlebarsService } from '../../../services/handlebars.service';
import { BlogPostService } from '../../../services/data/blog-post.service';
import { ActivatedRoute } from '@angular/router';
import { UtilService } from '../../../services/util.service';
import { DefaultData } from '../../../shared/const/defaultData';
import { isUndefined } from 'util';
import { GeneralConfig } from '../../../shared/const/config';
import { Util } from '../../../shared/const/util';
import { Templates } from '../../../shared/const/templates';
import { BlogPost } from '../../../shared/const/interfaces/data';

@Component({
  selector: 'app-post-display',
  templateUrl: './post-display.component.html',
  styleUrls: ['./post-display.component.css'],
})
export class PostDisplayComponent implements OnInit {
  posts: string[] = [];
  targetYear: number;
  targetMonth: number;
  targetId: string;

  constructor(private handlebarsService: HandlebarsService,
              private blogPostService: BlogPostService,
              private activatedRoute: ActivatedRoute,
              private utilService: UtilService) { }

  ngOnInit() {
    this.activatedRoute.params.subscribe((params) => {
      this.targetYear = +params[DefaultData.Keys.YEAR];
      this.targetMonth = Util.MonthSHTranslator.indexOf(params[DefaultData.Keys.MONTH]);
      this.targetId = params[DefaultData.Keys.ID];

      this.updateDisplay();
    });

    this.initializeObservables();
    this.updateDisplay();
  }

  initializeObservables(): void {
    this.blogPostService.postsDownloaded.subscribe(() => {
      this.updateDisplay();
    });

    // this.activatedRoute.params.subscribe((data) => {
    //   this.initializeData(data);
    // });
  }

  updateDisplay(): void {
    let tPosts: string[] = [];
    if (!this.hasYear()) {
      this.blogPostService.allPosts.some((blogPost: BlogPost, index: number) => {
        tPosts.push(this.handlebarsService.useCustomTemplate(Templates.BlogPost.id, blogPost));
        return index === GeneralConfig.Minimums.BlogPosts;
      });
    } else if (!this.hasMonth()) {
      this.blogPostService.posts[this.targetYear].forEach((blogPosts: BlogPost[]) => {
        blogPosts.forEach((blogPost: BlogPost) => {
          tPosts.push(this.handlebarsService.useCustomTemplate(Templates.BlogPost.id, blogPost));
        });
      });
      tPosts = tPosts.reverse();
    } else if (!this.hasId()) {
      this.blogPostService.posts[this.targetYear][this.targetMonth].forEach((blogPost: BlogPost) => {
        tPosts.push(this.handlebarsService.useCustomTemplate(Templates.BlogPost.id, blogPost));
      });
      tPosts = tPosts.reverse();
    } else {
      this.blogPostService.posts[this.targetYear][this.targetMonth].some((blogPost: BlogPost) => {
        if (blogPost.id === this.targetId) {
          tPosts.push(this.handlebarsService.useCustomTemplate(Templates.BlogPost.id, blogPost));
          return true;
        }
      });
    }
    this.posts = tPosts;
  }

  hasYear(): boolean {
    return !isNaN(this.targetYear);
  }

  hasMonth(): boolean {
    return this.targetMonth !== -1;
  }

  hasId(): boolean {
    return this.targetId !== undefined;
  }
}
