import { Component, OnInit } from '@angular/core';
import { BlogPostService, YearlyPosts } from '../../../services/data/blog-post.service';
import { isNullOrUndefined } from 'util';

@Component({
  selector: 'app-post-organizer',
  templateUrl: './post-organizer.component.html',
  styleUrls: ['./post-organizer.component.css']
})
export class PostOrganizerComponent implements OnInit {
  private addedHelpers = false;
  yearlyPosts: YearlyPosts;

  constructor(private blogPostService: BlogPostService) { }

  ngOnInit() {
    this.blogPostService.postsDownloaded.subscribe(() => {
      this.yearlyPosts = this.blogPostService.posts;
    });
    this.yearlyPosts = this.blogPostService.posts;
  }

  years(): number[] {
    if (isNullOrUndefined(this.yearlyPosts)) {
      return [];
    }
    return Object.keys(this.yearlyPosts).sort((a: string, b: string) => {
      if (Number.parseInt(a) > Number.parseInt(b)) {
        return -1;
      }
      return 1;
    }).map((element) => {
      return Number.parseInt(element);
    });
  }

  yearSelect(year: number): void {
    this.blogPostService.onYearSelect(year);
  }
}
