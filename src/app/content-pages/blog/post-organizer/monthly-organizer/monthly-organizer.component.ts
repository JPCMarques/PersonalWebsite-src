import { Component, OnInit, Input } from '@angular/core';
import { isNullOrUndefined } from 'util';
import { BlogPostService } from '../../../../services/data/blog-post.service';
import { Util } from '../../../../shared/const/util';
import { BlogPost } from '../../../../shared/const/interfaces/data';

@Component({
  selector: 'app-monthly-organizer',
  templateUrl: './monthly-organizer.component.html',
  styleUrls: ['./monthly-organizer.component.css']
})
export class MonthlyOrganizerComponent implements OnInit {
  @Input() monthlyData: BlogPost[][];
  @Input() year: number;

  MonthTranslator: string[] = Util.MonthTranslator;

  constructor(private blogPostService: BlogPostService) { }

  ngOnInit() {

  }

  months(): number[] {
    const months: number[] = [];
    this.monthlyData.forEach((monthPosts, index) => {
      if (!isNullOrUndefined(monthPosts)) {
        months.push(index);
      }
    });
    return months.reverse();
  }

  select(blogPost: BlogPost): void {
    this.blogPostService.onPostSelect(blogPost);
  }

  mSelect(month: number) {
    this.blogPostService.onMonthSelect(this.year, month);
  }

}

