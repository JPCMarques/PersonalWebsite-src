import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MonthlyOrganizerComponent } from './monthly-organizer.component';

describe('MonthlyOrganizerComponent', () => {
  let component: MonthlyOrganizerComponent;
  let fixture: ComponentFixture<MonthlyOrganizerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MonthlyOrganizerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MonthlyOrganizerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
