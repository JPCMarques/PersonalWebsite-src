import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';
}


interface ICustomActions {
  actions: ((input: any) => object)[];
}

const potato: ICustomActions = {
  actions: [
    (a: number) => ({test: 2}),
  ]
};
