import { Component, OnInit, Input } from '@angular/core';
import { Data } from '../const/interfaces/data';

@Component({
  selector: 'app-news-container',
  templateUrl: './news-container.component.html',
  styleUrls: ['./news-container.component.css']
})
export class NewsContainerComponent implements OnInit {

  @Input() newsList: Data[];

  constructor() {
  }

  ngOnInit() {
  }

}
