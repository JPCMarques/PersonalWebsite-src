import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { HandlebarsService } from '../../services/handlebars.service';
import { Templates } from '../const/templates';
import { Data } from '../const/interfaces/data';

@Component({
  selector: 'app-news-data',
  templateUrl: './news-data.component.html',
  styleUrls: ['./news-data.component.css']
})
export class NewsDataComponent implements OnInit {
  @Input() newsData: Data;
  @Output() newsClicked = new EventEmitter<Data>();

  newsCard: string;

  constructor(private handlebarsService: HandlebarsService) { }

  ngOnInit() {
    this.handlebarsService.addCustomTemplate(Templates.NewsCard.id, Templates.NewsCard.template);
    this.newsCard = this.handlebarsService.useCustomTemplate(Templates.NewsCard.id, this.newsData);
  }

}
