export abstract class DefaultData {
    public static readonly Services = {
        YEAR: new Date().getFullYear(),
        MONTH: new Date().getMonth(),
    };

    public static readonly Keys = {
        YEAR: 'year',
        MONTH: 'month',
        ID: 'id'
    };
}
