import * as firebase from 'firebase';
import 'firebase/firestore';

export interface Data {
    title: string;
    date: firebase.firestore.Timestamp;
    tags: string[];
    content: string;
    links?: Link[];
    id?: string; // Must be optional as a workaround to add id after parsing workaround.
}

export interface Link {
    display: string;
    destination: string;
}

export interface BlogPost extends Data {
    subtitle: string;
}

export interface Novel extends Data {
    wattpadLink: string;
    smapKey: string;
}

export interface App extends Data {
    srcCodeLink?: string;
}

export interface ImgData {
    id: string;
    url: string;
}

export interface ITemplate {
    id: string;
    template: string;
}

export interface ITemplateable {
    getTemplateID: () => string;
    getTemplate: () => string;
}

export interface ISMapMetadataCollection {
    [index: string]: ISMapMetadata;
}

export interface ISMapMetadata {
    levels: ILevelData[];
}

export interface ILevelData {
    name: string;
    isCollapsed?: boolean;
    members?: (string | ILevelData)[];
}

export interface ISMapCollection {
    [index: string]: ISMap;
}

export interface ISMap {
    title: string;
    levelId?: string; // Same id as container level; included here for ease of use.
    descriptor?: string;
    content: string;
    subLevels?: ISMapLevel[];
}

export interface ISMapLevel {
    id: string;
    members: ISMap[];
}

export interface IDestructiveCopyCfg {
    blackList?: string[];
    clearList?: string[];
}
