export abstract class GeneralConfig {
    public static readonly Minimums = {
        BlogPosts: 5,
    };
}

export abstract class FirebaseConfig {
    public static readonly Collections = {
        Novels: 'novels',
        BlogPosts: 'blogPosts',
        Apps: 'apps',
        NovelNews: 'news/novels/updates',
        GeneralNews: 'news/general/news',
        StoryMapMeta: 'smapMetadata',
        StoryMaps: 'smaps'
    };

    public static readonly ConstantKeys = {
        Novels: {
            BookHunters: 'Book Hunters',
        }
    };
}
