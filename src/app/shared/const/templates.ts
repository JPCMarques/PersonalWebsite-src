import { ITemplate } from './interfaces/data';

export abstract class Templates {
    public static readonly BlogPost: ITemplate = {
        id: 'BPOSTSRC',
        template: `<div>
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h1>{{title}}</h1>
                        <h4 class="text-secondary">{{subtitle}}</h4>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 text-justify">
                        {{trustedHTML (recomp content)}}
                        {{#if links}}<h5>Links</h5>
                        <ul>
                        {{#each links}}
                            <li><a href="{{destination}}">{{display}}</a> </li>
                        {{/each}}
                        </ul>
                        {{/if}}
                        {{#if tags}}<p class="text-center text-muted">{{tagsFormatter tags}}</p>{{/if}}
                        {{#if date}}<p class="text-right text-muted">Posted/Created at: {{sDate (fstoreDate date)}}</p>{{/if}}
                    </div>
                </div>
            </div>
        </div>`
    };

    public static readonly App: ITemplate = {
        id: 'APPSRC',
        template: `<div>
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h1>{{title}}</h1>
                        {{#if srcCodeLink}}<h4 class="text-secondary"><a href="{{srcCodeLink}}">Src</a></h4>{{/if}}
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        {{trustedHTML content}}
                        <br/>
                        <br/>
                        {{#if links}}<h5>Links</h5>
                        <ul>
                        {{#each links}}
                            <li><a href="{{destination}}">{{display}}</a> </li>
                        {{/each}}
                        </ul>
                        {{/if}}
                        {{#if tags}}<p class="text-center text-muted">{{tagsFormatter tags}}</p>{{/if}}
                        {{#if date}}<p class="text-right text-muted">Posted/Created at: {{sDate (fstoreDate date)}}</p>{{/if}}
                    </div>
                </div>
            </div>
        </div>`
    };

    public static readonly Novel: ITemplate = {
        id: 'NOVELSRC',
        template: `<div>
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h1>{{title}}</h1>
                        {{#if wattpadLink}}<h4 class="text-secondary" style="float:left;"><a href="{{wattpadLink}}">Wattpad</a></h4>{{/if}}
                        {{#if smapKey}}<h4 class="text-secondary"><a href="{{smapKey}}">Story Map</a></h4>{{/if}}
                        <br/>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        {{trustedHTML content}}
                        {{#if links}}
                        <br/><h5>Links</h5>
                        <ul>
                        {{#each links}}
                            <li><a href="{{destination}}">{{display}}</a> </li>
                        {{/each}}
                        </ul>
                        {{/if}}
                        <br/>
                        <br/>
                        {{#if tags}}<p class="text-center text-muted">{{tagsFormatter tags}}</p>{{/if}}
                        {{#if date}}<p class="text-right text-muted">Posted/Created at: {{sDate (fstoreDate date)}}</p>{{/if}}
                    </div>
                </div>
            </div>
        </div>`
    };

    public static readonly NewsCard: ITemplate = {
        id: 'NCARDSRC',
        template: `<div class="card">
            <div class="card-body">
                <h4 class="card-title">{{title}}</h4>
                <p class="card-text">
                {{trustedHTML (recomp content)}}
                {{#if links}}
                <br/><h5>Links</h5>
                <ul>
                {{#each links}}
                    <li><a href="{{destination}}">{{display}}</a> </li>
                {{/each}}
                </ul>
                {{/if}}
                <br/>
                <br/>
                {{#if date}}<p class="text-right text-muted">{{sDate (fstoreDate date)}}</p>{{/if}}
            </div>
        </div>`
    };

    public static readonly StoryMap: ITemplate = {
        id: 'SMAPSRC',
        template: `
            <h1>{{title}}</h1>
            <h3 class="text-muted mb-2">{{descriptor}}</h3>
            <p>{{content}}</p>
            {{#if subpages}}
                <div class="row">
                    {{#each subpages}}
                        <div class="col-lg-3" style="margin-top: 5px; margin-bottom: 5px;">
                            <div class="card">
                                <div class="card-body">
                                    <h4 class="card-title">{{title}}</h4>
                                    <h6 class="text-muted card-subtitle mb-2">{{descriptor}}</h6>
                                </div>
                            </div>
                        </div>
                    {{/each}}
                </div>
            {{/if}}`
    };

    public static readonly SMapSubpage: ITemplate = {
        id: 'SMAPSLSRC',
        template: `
        <div class="col-lg-3" style="margin-top: 5px; margin-bottom: 5px;">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">{{title}}</h4>
                    <h6 class="text-muted card-subtitle mb-2">{{descriptor}}</h6>
                </div>
            </div>
        </div>
        `
    };
}
