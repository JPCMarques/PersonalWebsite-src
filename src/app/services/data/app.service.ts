import { Injectable, EventEmitter, Output } from '@angular/core';
import { DataService } from '../data.service';
import { UtilService } from '../util.service';
import { HandlebarsService } from '../handlebars.service';
import { Templates } from '../../shared/const/templates';
import { App } from '../../shared/const/interfaces/data';
import { FirebaseConfig } from '../../shared/const/config';

@Injectable()
export class AppService {
  @Output() appDataUpdated = new EventEmitter();

  apps: App[];

  constructor(private dataService: DataService, private utilService: UtilService, private handlebarsService: HandlebarsService) {
    this.dataService.registerCollection(FirebaseConfig.Collections.Apps);
    this.handlebarsService.addCustomTemplate(Templates.App.id, Templates.App.template);
    this.init();
  }

  private init(): void {
    this.dataService.accessCollection(FirebaseConfig.Collections.Apps).orderBy('date', 'desc').onSnapshot((apps) => {
      const tApps = [];
      apps.forEach((doc) => {
        const data = doc.data() as App;
        data.id = doc.id;
        tApps.push(data);
      });
      this.apps = tApps;
      this.appDataUpdated.emit();
    });
  }
}
