import { Injectable, Output, EventEmitter } from '@angular/core';
import { DataService } from '../data.service';
import { UtilService } from '../util.service';
import { isNullOrUndefined, isNull } from 'util';
import { Router } from '@angular/router';
import { Util } from '../../shared/const/util';
import { HandlebarsService } from '../handlebars.service';
import { Templates } from '../../shared/const/templates';
import { BlogPost } from '../../shared/const/interfaces/data';
import { FirebaseConfig } from '../../shared/const/config';

@Injectable()
export class BlogPostService {
  @Output() postsDownloaded = new EventEmitter();
  @Output() postSelected = new EventEmitter<BlogPost>();
  @Output() monthSelected = new EventEmitter<BlogPost[]>();
  @Output() yearSelected = new EventEmitter<BlogPost[][]>();

  public posts: YearlyPosts = {};
  public allPosts: BlogPost[] = [];
  public selectPost: BlogPost;

  private existingIDs = new Set<string>();

  constructor(private dataService: DataService, private utilService: UtilService,
              private router: Router, private handlebarsService: HandlebarsService) {
    this.dataService.registerCollection(FirebaseConfig.Collections.BlogPosts);
    this.handlebarsService.addCustomTemplate(Templates.BlogPost.id, Templates.BlogPost.template);
    this.getAllData();
  }

  private getAllData() {
    this.dataService.accessCollection(FirebaseConfig.Collections.BlogPosts).orderBy('date', 'desc').onSnapshot((blogPosts) => {
      const tPosts = [];
      blogPosts.forEach(doc => {
        this.existingIDs.add(doc.id);
        const blogData = doc.data() as BlogPost;
        blogData.id = doc.id;
        tPosts.push(blogData);
        this.addBlogData(blogData, doc.id);
      });
      this.allPosts = tPosts;
      this.postsDownloaded.emit();

      const yearData = this.posts[+Object.keys(this.posts).sort(this.utilService.yearReverse)[0]];
      for (let i = 11; i >= 0; i--) {
        if (!isNullOrUndefined(yearData[i])) {
          this.monthSelected.emit(yearData[i]);
          break;
        }
      }
    });
  }

  private addBlogData(blogData: BlogPost, id: string): void {
    const date = blogData.date.toDate();
    let monthlyPosts: BlogPost[];
    if (isNullOrUndefined(this.posts[date.getFullYear()])) {
      this.posts[date.getFullYear()] = new Array<BlogPost[]>(12);
    }
    monthlyPosts = this.posts[date.getFullYear()][date.getMonth()];
    if (isNullOrUndefined(monthlyPosts)) {
      this.posts[date.getFullYear()][date.getMonth()] = [blogData];
    } else {
      if (this.existingIDs.has(id)) {
        monthlyPosts.forEach((post: BlogPost, index: number) => {
          if (post.date.toDate().getDate() === blogData.date.toDate().getDate()) {
            monthlyPosts[index] = blogData;
          }
        });
      } else {
        monthlyPosts.push(blogData);
        monthlyPosts.sort(this.utilService.blogSortReverse);
      }
    }
  }

  public onPostSelect(blogPost: BlogPost): void {
    this.postSelected.emit(blogPost);
    this.router.navigate(['blog', blogPost.date.toDate().getFullYear(),
      Util.MonthSHTranslator[blogPost.date.toDate().getMonth()],
      blogPost.id]);
  }

  public onMonthSelect(year: number, month: number): void {
    this.monthSelected.emit(this.posts[year][month]);
    this.router.navigate(['blog', year, Util.MonthSHTranslator[month]]);
  }

  public onYearSelect(year: number): void {
    this.yearSelected.emit(this.posts[year]);
    this.router.navigate(['blog', year]);
  }
}

export interface YearlyPosts {
  [index: number]: BlogPost[][];
}
