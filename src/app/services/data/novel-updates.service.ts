import { Injectable, Output, EventEmitter } from '@angular/core';
import { DataService } from '../data.service';
import { Data } from '../../shared/const/interfaces/data';
import { FirebaseConfig } from '../../shared/const/config';

@Injectable()
export class NovelUpdatesService {
  @Output() novelUpdatesUpdated = new EventEmitter();

  novelUpdates: Data[];

  constructor(private dataService: DataService) {
    this.dataService.registerCollection(FirebaseConfig.Collections.NovelNews);
    this.init();
  }

  private init(): void {
    this.dataService.accessCollection(FirebaseConfig.Collections.NovelNews).orderBy('date', 'desc').onSnapshot((novelUpdates) => {
      const tUpdates = [];
      novelUpdates.forEach((doc) => {
        const data = doc.data() as Data;
        data.id = doc.id;
        tUpdates.push(data);
      });
      this.novelUpdates = tUpdates;
      this.novelUpdatesUpdated.emit();
    });
  }
}
