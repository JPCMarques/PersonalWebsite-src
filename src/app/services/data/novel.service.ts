import { Injectable, EventEmitter, Output } from '@angular/core';
import { DataService } from '../data.service';
import { HandlebarsService } from '../handlebars.service';
import { Templates } from '../../shared/const/templates';
import { Novel } from '../../shared/const/interfaces/data';
import { FirebaseConfig } from '../../shared/const/config';

@Injectable()
export class NovelService {
  private static blackListedIDs = ['news'];
  @Output() novelsUpdated = new EventEmitter();

  novels: Novel[];

  constructor(private dataService: DataService, private handlebarsService: HandlebarsService) {
    this.dataService.registerCollection(FirebaseConfig.Collections.Novels);
    this.handlebarsService.addCustomTemplate(Templates.Novel.id, Templates.Novel.template);
    this.dataService.accessCollection(FirebaseConfig.Collections.Novels).orderBy('date', 'desc').onSnapshot((apps) => {
      const tNovels = [];
      apps.forEach((doc) => {
        if (NovelService.blackListedIDs.indexOf(doc.id) === -1) {
          const data = doc.data() as Novel;
          data.id = doc.id;
          tNovels.push(data);
        }
      });
      this.novels = tNovels;
      this.novelsUpdated.emit();
    });
  }
}
