import { Injectable, Output, EventEmitter } from '@angular/core';
import { DataService } from '../data.service';
import { Data } from '../../shared/const/interfaces/data';
import { FirebaseConfig } from '../../shared/const/config';

@Injectable()
export class GeneralNewsService {
  @Output() newsUpdated = new EventEmitter();

  news: Data[];

  constructor(private dataService: DataService) {
    this.dataService.registerCollection(FirebaseConfig.Collections.GeneralNews);
    this.init();
  }

  private init(): void {
    this.dataService.accessCollection(FirebaseConfig.Collections.GeneralNews).orderBy('date', 'desc').limit(5).onSnapshot((news) => {
      const tNews = [];
      news.forEach((doc) => {
        const data = doc.data() as Data;
        data.id = doc.id;
        tNews.push(data);
      });
      this.news = tNews;
      this.newsUpdated.emit();
    });
  }
}
