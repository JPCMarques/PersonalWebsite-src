import { Injectable, EventEmitter, Output } from '@angular/core';
import { DataService } from '../../data.service';
import { ISMapCollection, ISMap} from '../../../shared/const/interfaces/data';
import { FirebaseConfig } from '../../../shared/const/config';
import { HandlebarsService } from '../../handlebars.service';
import { Templates } from '../../../shared/const/templates';

@Injectable({
    providedIn: 'root'
})
export class StoryMapService {
    @Output() storyMapsUpdated = new EventEmitter();

    private storyMaps: ISMapCollection = {};

    constructor(private dataService: DataService, private handlebarsService: HandlebarsService) {
        this.handlebarsService.addCustomTemplate(Templates.StoryMap.id, Templates.StoryMap.template);
        this.dataService.registerCollection(FirebaseConfig.Collections.StoryMaps);
        this.dataService.accessCollection(FirebaseConfig.Collections.StoryMaps).onSnapshot((snapshot) => {
            const tSMC: ISMapCollection = {};
            snapshot.forEach((doc) => {
                const sMap = doc.data() as ISMap;
                tSMC[doc.id] = sMap;
            });
            this.storyMaps = tSMC;
            this.storyMapsUpdated.emit();
        });
    }

    public getStoryMap(id: string): ISMap {
        return this.storyMaps[id];
    }
}
