import { Injectable, Output, EventEmitter } from '@angular/core';
import { DataService } from '../../data.service';
import { FirebaseConfig } from '../../../shared/const/config';
import { ISMapMetadataCollection, ISMapMetadata, ILevelData } from '../../../shared/const/interfaces/data';

@Injectable({
    providedIn: 'root'
})
export class StoryMapMetadataService {
    private currentSmap: ISMapMetadata;
    private smapMetaCollection: ISMapMetadataCollection = {};
    private lastCollapsedCoord: string;

    @Output() smapMetaCollectionUpdated = new EventEmitter();
    @Output() smapMetaCollapseID = new EventEmitter<string>();

    constructor(private dataService: DataService) {
        this.dataService.registerCollection(FirebaseConfig.Collections.StoryMapMeta);
        this.dataService.accessCollection(FirebaseConfig.Collections.StoryMapMeta).onSnapshot((snapshot) => {
            const tSMMC: ISMapMetadataCollection = {};
            snapshot.forEach((doc) => {
                const smapMeta = doc.data() as ISMapMetadata;
                tSMMC[doc.id] = smapMeta;
            });
            this.smapMetaCollection = tSMMC;
            this.smapMetaCollectionUpdated.emit();
        });
    }

    public getMetadata(id: string): ISMapMetadata {
        return (this.currentSmap = this.smapMetaCollection[id]);
    }

    public handleCollapse(id: string): void {
        this.setCollapse(this.lastCollapsedCoord, false);
        this.setCollapse(id, true, true);
        this.smapMetaCollapseID.emit(id);
        this.lastCollapsedCoord = id;
    }

    private setCollapse(id: string, collapseValue: boolean, swapLast = false): void {
        console.log(id);
        console.log(this.currentSmap);
        if (id) {
            const targets = id.slice(1).split('|');
            let first: ILevelData;
            for (let i = 0; i < targets.length; i++) {
                first = first ? first.members[i] : this.currentSmap.levels[targets[0]];
                if (i === targets.length - 1 && swapLast) {
                    first.isCollapsed = !first.isCollapsed;
                } else {
                    first.isCollapsed = collapseValue;
                }
            }
        }
    }
}
