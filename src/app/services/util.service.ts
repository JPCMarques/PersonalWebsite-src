import { Injectable } from '@angular/core';
import { BlogPost, ISMap, IDestructiveCopyCfg } from '../shared/const/interfaces/data';

@Injectable()
export class UtilService {
    public imgUrlCache: { [index: string]: string } = {};

    public blogSortReverse = (a: BlogPost, b: BlogPost) => {
        if (a.date.valueOf() < b.date.valueOf()) {
            return 1;
        } else if (a.date.valueOf() > b.date.valueOf()) {
            return -1;
        } else {
            return 0;
        }
    }

    public yearReverse = (a: string, b: string) => {
        if (+a > +b) {
            return -1;
        } else {
            return 1;
        }
    }

    public monthShort(month: number): string {
        let base = '';
        if (month < 9) {
            base = '0';
        }
        return base + (month + 1);
    }

    public idFromPath(path: string): string {
        return path.replace('/', '-').replace('.', '');
    }

    public queryParamsFromSMap(smap: ISMap): object {
        const qParams = {};
        qParams[smap.levelId] = smap.title;
        return qParams;
    }

    public appendQueryParams(queryParams: object, smap: ISMap): object {
        const tQueryParams = {...queryParams};
        tQueryParams[smap.levelId] = smap.title;
        return tQueryParams;
    }

    public destructiveCopy(src: object, dest: object, config?: IDestructiveCopyCfg) {
        if (config.clearList !== undefined) {
            config.clearList.forEach((clear) => {
                dest[clear] = undefined;
            });
        }

        Object.keys(src).forEach((key) => {
            if (config.blackList === undefined || config.blackList.indexOf(key) === -1) {
                dest[key] = src[key];
            }
        });
    }

    constructor() { }

}
