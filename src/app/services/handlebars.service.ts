import { Injectable } from '@angular/core';
import * as Handlebars from 'handlebars/dist/handlebars';
import { DataService } from './data.service';
import { UtilService } from './util.service';
import { isNullOrUndefined } from 'util';
import { firestore } from 'firebase';
import { ImgData, ITemplateable } from '../shared/const/interfaces/data';

@Injectable()
export class HandlebarsService {
    private customTemplates: CustomTemplates = {};

    constructor(private utilService: UtilService, private dataService: DataService) {
        this.initializeHandlebarsHelpers(utilService, dataService);
    }

    private initializeHandlebarsHelpers(utilSvc: UtilService, dataSvc: DataService): void {
        Handlebars.registerHelper('tagsFormatter', function (items: string[]) {
            return items.join(', ');
        });

        Handlebars.registerHelper('sDate', function (date: Date) {
            const dateString = `${date.getDate()}/${utilSvc.monthShort(date.getMonth())}/${date.getFullYear()}`;
            return dateString;
        });

        Handlebars.registerHelper('fstoreDate', function (date: firestore.Timestamp) {
            return date.toDate();
        });

        Handlebars.registerHelper('trustedHTML', function (html) {
            return new Handlebars.SafeString(html);
        });

        Handlebars.registerHelper('recomp', function (data) {
            const dataTemplate = Handlebars.compile(data);
            const recomped = dataTemplate(data);
            return new Handlebars.SafeString(recomped);
        });

        Handlebars.registerHelper('imgDL', function (path: string) {
            const id = utilSvc.idFromPath(path);
            const url = utilSvc.imgUrlCache[id];
            const imgHTML = `<br><img id="${id}" src="${isNullOrUndefined(url) ? '' : url}" style="max-width: 100%">`;
            if (isNullOrUndefined(url)) {
                dataSvc.download(path);
            }
            return new Handlebars.SafeString(imgHTML);
        });

        dataSvc.imgDownloaded.subscribe((imgData: ImgData) => {
            const img = document.getElementById(imgData.id) as HTMLImageElement;
            img.src = imgData.url;
            utilSvc.imgUrlCache[imgData.id] = imgData.url;
        });
    }

    public addCustomTemplate(id: string, templateSrc: string) {
        this.customTemplates[id] = Handlebars.compile(templateSrc);
    }

    public addTemplateable(templateable: ITemplateable) {
        this.addCustomTemplate(templateable.getTemplateID(), templateable.getTemplate());
    }

    public useCustomTemplate(id: string, input: any): string {
        return this.customTemplates[id](input);
    }

    public useTemplateable(templateable: ITemplateable, input: any): string {
        return this.useCustomTemplate(templateable.getTemplateID(), input);
    }

    public addCustomHelper(name: string, fn: Handlebars.HelperDelegate) {
        Handlebars.registerHelper(name, fn);
    }

}

export interface CustomTemplates {
    [index: string]: Handlebars.TemplateDelegate;
}
