import { Injectable, EventEmitter } from '@angular/core';
import * as firebase from 'firebase';
import 'firebase/firestore';
import 'firebase/storage';
import { UtilService } from './util.service';
import { ImgData } from '../shared/const/interfaces/data';
import { FirebaseConfig } from '../shared/const/config';

@Injectable()
export class DataService {

    private config = {
        apiKey: 'AIzaSyDR6z5Jogk6FR81CHEImJ6Uz3LcNFsQqP0',
        authDomain: 'jpcmarques-website.firebaseapp.com',
        databaseURL: 'https://jpcmarques-website.firebaseio.com',
        projectId: 'jpcmarques-website',
        storageBucket: 'jpcmarques-website.appspot.com',
        timestampsInSnapshots: true
    };

    private firebase: firebase.app.App;
    private collections: { [index: string]: firebase.firestore.CollectionReference};
    public firestore: firebase.firestore.Firestore;
    public storage: firebase.storage.Storage;

    public imgDownloaded = new EventEmitter<ImgData>();

    constructor(private utilService: UtilService) {
        this.firebase = firebase.initializeApp(this.config);
        this.firestore = this.firebase.firestore();
        this.firestore.settings({ timestampsInSnapshots: true });
        this.storage = this.firebase.storage();
        this.collections = {};
    }

    public registerCollection(collectionKey: string): void {
        console.log('Registered collection with key: ' + collectionKey);
        this.collections[collectionKey] = this.firestore.collection(collectionKey);
    }

    public accessCollection(key: string): firebase.firestore.CollectionReference {
        return this.collections[key];
    }

    public download(path: string): void {
        const event = this.imgDownloaded;
        this.storage.ref(path).getDownloadURL().then((url) => {
            event.emit({ id: this.utilService.idFromPath(path), url: url });
        }).catch(function (error) {
            // Handle any errors
        });
    }
}
