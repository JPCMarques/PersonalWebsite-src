import { Injectable } from '@angular/core';
import * as uuid from 'uuid';

@Injectable({
  providedIn: 'root'
})
export class UUIDService {
  private lastUUID: string;

  constructor() {
    this.generate();
  }

  public generate(): string {
    return (this.lastUUID = uuid.v4());
  }

  public last(): string {
    return this.lastUUID;
  }
}
